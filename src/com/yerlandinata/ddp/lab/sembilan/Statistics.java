package com.yerlandinata.ddp.lab.sembilan;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * List yang itemnya dapat berupa apa saja
 * Dapat memproses data dalam bilangan riil apapun selama MUAT pada memori komputer
 * @author Yudhistira Erlandinata
 */
class Statistics {
    private List<String> dataList;
    private PrintWriter log;
    private static final String ERROR_NUMFORMAT = "Error = Invalid number format at index %d%n";
    private static final String ERROR_NO_VALID_NUM = "Error = Tidak ada bilangan yang valid pada index %d — %d%n";

    /**
     * Constructor Statistics hanya butuh list berisi string
     * @param dataList List of String
     */
    Statistics(List<String> dataList, PrintWriter log) {
        this.log = log;
        this.dataList = dataList;
    }

    /**
     * Mencari nilai minimum dari list yang diberikan antara index idxA sampai idxB inklusif
     * @param idxA Index awal
     * @param idxB Index akhir
     * @return Nilai minimum
     */
    BigDecimal findMinimum(int idxA, int idxB) throws IOException {
        BigDecimal minimum = BigDecimal.ZERO; //syarat compiler yang rewel
        boolean isMinimumSet = false;
        for(int i = idxA-1; i < idxB; i++){
            BigDecimal deci;
            try {
                deci = new BigDecimal(dataList.get(i));
                if (!isMinimumSet){
                    minimum = deci;
                    isMinimumSet = true;
                }
            } catch (NumberFormatException e) {
                log.printf(ERROR_NUMFORMAT, i+1);
                continue;
            }
            minimum = deci.min(minimum); //transitif
        }
        if (!isMinimumSet)
            throw new IOException(String.format(ERROR_NO_VALID_NUM, idxA, idxB));
        return minimum;
    }

    /**
     * Mencari nilai rata-rata pada list yang ada dari index idxA sampai idxB inklusif, one-based index
     * @param idxA Index kiri
     * @param idxB Index kanan
     * @return mean
     */
    BigDecimal findMean(int idxA, int idxB){
        BigDecimal sum = BigDecimal.ZERO;
        int validDataCount = 0;
        for(int i = idxA-1; i <= idxB-1; i++){
            try{
                sum = sum.add(new BigDecimal(dataList.get(i)));
                validDataCount++;
            } catch(NumberFormatException e){
                log.printf(ERROR_NUMFORMAT, i+1);
            }
        }
        if (validDataCount == 0)
            return BigDecimal.ZERO;
        else
            return sum.divide(new BigDecimal(validDataCount), 10, BigDecimal.ROUND_HALF_DOWN);
    }

    /**
     * Mencari nilai tengan pada list yang ada dari index idxA sampai idxB inklusif
     *  dan list tersebut akan disort, one-based index
     * @param idxA Index kiri
     * @param idxB Index kanan
     * @return median
     */
    BigDecimal findMedian(int idxA, int idxB) throws IOException{
        List<BigDecimal> deciList = new ArrayList<>();
        for(int i=idxA-1; i<=idxB-1; ++i){
            try{
                deciList.add(new BigDecimal(dataList.get(i)));
            }catch(NumberFormatException e){
                log.printf(ERROR_NUMFORMAT, i+1);
            }
        }
        if (deciList.size() == 0){
            throw new IOException(String.format(ERROR_NO_VALID_NUM, idxA, idxB));
        }
        Collections.sort(deciList);
        if ((deciList.size() & 1) == 1) //ganjil
            return deciList.get(deciList.size()/2);
        else
            return (deciList.get(deciList.size()/2).add(deciList.get((deciList.size()/2) - 1))).divide(BigDecimal.ONE.add(BigDecimal.ONE), 10, BigDecimal.ROUND_HALF_DOWN);
    }

    /**
     * Mencari nilai varian pada list yang ada dari index idxA sampai idxB inklusif, one-based index
     * @param idxA Index kiri
     * @param idxB Index kanan
     * @return mean
     */
    BigDecimal findVariance(int idxA, int idxB) throws IOException{
        BigDecimal mean = findMean(idxA, idxB);
        BigDecimal diffSum = BigDecimal.ZERO;
        int validDataCount = 0;
        for(int i = idxA-1; i < idxB; i++){
            try {
                BigDecimal diff = (new BigDecimal(dataList.get(i))).subtract(mean);
                diffSum = diffSum.add(diff.multiply(diff));
                validDataCount++;
            } catch (NumberFormatException e) {
                log.printf(ERROR_NUMFORMAT, i+1);
            }
        }
        if (validDataCount == 0)
            throw new IOException(String.format(ERROR_NO_VALID_NUM, idxA, idxB));
        return diffSum.divide(new BigDecimal(validDataCount), 10, BigDecimal.ROUND_HALF_DOWN);
    }

}
