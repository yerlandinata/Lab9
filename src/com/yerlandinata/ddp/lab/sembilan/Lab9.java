package com.yerlandinata.ddp.lab.sembilan;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Lab9 DDP 2
 * @author Yudhistira Erlandinata
 */
public class Lab9 {

    private static int readIntFromFile(Scanner in, PrintWriter out, String nonIntMsg, String notExistMsg) throws NoSuchElementException{
        int result;
        try{
            result = in.nextInt();
        }catch (InputMismatchException e){
            out.printf("%s%n", nonIntMsg);
            throw e;
        }catch (NoSuchElementException e){
            out.printf("%s%n", notExistMsg);
            throw e;
        }
        return result;
    }

    public static void main(String[] args) {
	    final String INPUT_FILE = "input.in";
	    final String OUTPUT_FILE = "output.out";
        final String ERROR_MUST_INT = "Error = Index harus bilangan bulat";
        final String ERROR_OPERAND_MISSING = "Error = File kekurangan operand operasi";
        final String MIN = "min";
        final String MEDIAN = "median";
        final String MEAN = "mean";
        final String VARIANCE = "varian";
        FileReader reader;
        try {
            reader = new FileReader(INPUT_FILE);
        } catch (FileNotFoundException e) {
            System.out.printf("Error = File %s not found", INPUT_FILE);
            return;
        }
        PrintWriter out;
        try {
            out = new PrintWriter(OUTPUT_FILE);
        } catch (FileNotFoundException e) {
            System.out.println("Error = Sistem gagal membuka file output " + OUTPUT_FILE);
            return;
        }
        Scanner in = new Scanner(reader);
        in.useDelimiter("\\s+"); //spasi berapa pun
        int listSize;
        try {
            listSize = readIntFromFile(in, out, String.format("Error = File %s tidak memberikan ukuran list dalam bentuk bilangan bulat", INPUT_FILE), "Error = File kosong.");
            in.nextLine();
        }catch (NoSuchElementException e){
            return;
        }
        if (!in.hasNextLine()){
            out.printf("Error = File %s tidak memberikan baris data%n", INPUT_FILE);
            return;
        }
        String dataLine = in.nextLine();
        //linkedlist support removing item
        List<String> dataList = new LinkedList<>(Arrays.asList(dataLine.split("\\s+")));
        if (dataList.size() == 0){
            out.println("Error = Baris data kosong");
            return;
        }
        if (dataList.get(0).equals(""))
            dataList.remove(""); //remove leading space
        if (dataList.size() != listSize){
            out.printf("Error = File memberikan list berukuran %d, sedangkan sebelumnya menyatakan ukuran list adalah %d%n", dataList.size(), listSize);
            listSize = dataList.size(); //actual list size
            out.printf("Asumsi program: Sekarang ukuran list menjadi %d%n", listSize);
        }
        Statistics statistics = new Statistics(dataList, out);
        int numberOfOperations;
        try {
            numberOfOperations = readIntFromFile(in, out, String.format("Error = File %s tidak memberikan banyak operasi dalam bentuk bilangan bulat", INPUT_FILE), String.format("Error = File %s tidak menspesifikasikan banyak operasi.", INPUT_FILE));
            in.nextLine();
        }catch (NoSuchElementException e){
            return;
        }
        for(int i = 0; i < numberOfOperations; i++){
            if (!in.hasNextLine()){
                out.printf("Error = File menyatakan bahwa banyak operasi adalah %d operasi, tapi file hanya memberikan %d operasi.%n", numberOfOperations, i);
                break;
            }
            String[] opLine = in.nextLine().split("\\s+");
            int start = 0;
            if (opLine[0].equals("")) //leading space
                start = 1;
            String operation = opLine[start];
            int operandA;
            int operandB;
            try {
                operandA = Integer.parseInt(opLine[start+1]);
                operandB = Integer.parseInt(opLine[start+2]);
            } catch (NumberFormatException e) {
                out.println(ERROR_MUST_INT);
                continue;
            } catch (IndexOutOfBoundsException e){
                out.println(ERROR_OPERAND_MISSING);
                continue;
            }
            if (operandB > listSize){
                out.printf("Error = Index out of bounds; Y = %d, ukuran list sebenarnya = %d (bisa jadi kurang dari yang dispesifikasikan file karena kesalahan format file)%n", operandB, listSize);
                continue;
            }
            try {
                switch (operation.toLowerCase()){
                    case MIN:
                        out.printf("Nilai minimum dari index ke %d — %d adalah %s%n", operandA, operandB, statistics.findMinimum(operandA, operandB).toString());
                        break;
                    case MEDIAN:
                        out.printf("Nilai median dari index ke %d — %d adalah %s%n", operandA, operandB, statistics.findMedian(operandA, operandB).toString());
                        break;
                    case MEAN:
                        out.printf("Nilai rata-rata dari index ke %d — %d adalah %s%n", operandA, operandB, statistics.findMean(operandA, operandB).toString());
                    case VARIANCE:
                        out.printf("Nilai varian dari index ke %d — %d adalah %s%n", operandA, operandB, statistics.findVariance(operandA, operandB).toString());
                        break;
                    default:
                        out.printf("Error = Can not find operation \"%s\"%n", operation);
                }
            } catch (IOException e) {
                out.print(e.getMessage());
            }
        }
        in.close();
        out.close();
    }
}
